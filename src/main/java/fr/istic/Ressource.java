package fr.istic;

import java.util.ArrayList;
import java.util.List;

public class Ressource{

    public static List<Techno> technos() {
        ArrayList<Techno> res = new ArrayList<>();
        String zk = "ZK est un framework OpenSource proposant une interaction utilisateur riche. Il est possible de définir des interfaces graphiques via une syntaxe XML, le ZUML (langage de balise, basé sur le standard XUL de Mozilla) plus précisement ou un éditeur WYSIWYG que de manipuler directement des objets java. Il propose l’avantage par rapport à d’autre framework web de ne pas mélanger plusieurs langages de programmation, içi seul le JAVA est utilisé. Ce framework permet de créer des applications selon le modèle MVC.";
        String zkUrl = "https://docs.google.com/document/d/11wO7ODEYMiIAEyRop-teJ7DLSlo47V3K1St9n1elOeM/edit?usp=sharing";
        res.add(new Techno("zk", zk, zkUrl));
        String vexi = "The Vexi project is an international effort to create an easy-to-use platform for the development and delivery of Internet application interfaces outside of the standard browser stack. It has similarities with XUL but runs on top of the Java stack, making it browser independent.";
        String vexiUrl = "https://docs.google.com/document/d/1e6GZx31cPO5hygAjGIN9LWN_78aYvJXxUzDgzq8uPj0/edit?usp=sharing";
        res.add(new Techno("Vexi", vexi, vexiUrl));
        String FXML = "FXML est basé sur sur le XML, et a été créé par Oracle. Il a été réalisé pour la création d'interface homme machine d'une application JavaFX. Puisque le graphe de scene d'une application JavaFX est très proche du XML, FXML permet de faire le pont entre les deux.";
        String FXMLUrl = "https://docs.google.com/document/d/1vUC_Qgm_hHr6VVP7ILPs3oUTla5WnK_vHPlUfAQp_kM/edit?usp=sharing";
        res.add(new Techno("fxml", FXML, FXMLUrl));
        String mxml = "Le MXML est le langage de description développé par Macromedia, puis repris par Adobe Systems pour la plateforme Adobe Flex. Il est dérivé du XML et permet de décrire la présentation des interfaces utilisées dans le cadre du développement des clients riches ou RIA (Rich Internet Application).";
        String mxmlUrl ="https://docs.google.com/document/d/1HMMcMWqi_QwRSlYF40W2IjzNBIjIsISuul7j46AmUoM/edit?usp=sharing";
        res.add(new Techno("mxml", mxml, mxmlUrl));
        String xaml = "Le XAML est un langage utilisé pour initialiser des objets et données structurés en C#. Ses usages ne sont pas limités à gérer des interfaces graphique mais il est utilisé dans les deux frameworks graphiques principaux de .net : WPF WinForms.";
        String xamlUrl = "https://docs.google.com/document/d/1Sfochd6bqJtA0S5HN_2qXCKXToaruFh8efl7U2WXwhI/edit?usp=sharing";
        res.add(new Techno("xaml", xaml, xamlUrl));
        return res;
    }
}
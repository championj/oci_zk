package fr.istic;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

public class MyViewModel extends SelectorComposer<Component> {

    private ListModelList<Techno> technos;

    public MyViewModel() {
        super();
        technos = new ListModelList<>(Ressource.technos(), true);
    }

    @Wire
    private Button addBtn;

    @Wire
    private Textbox technoEditableText;

    @Wire
    private Textbox technoEditableTextDescription;

    @Wire
    private Textbox technoEditableTextUrl;

    @Wire
    private Listbox technoList;

    @Wire
    private Label nameTechno;

    @Wire
    private Label descriptionTechno;

    @Wire
    private Label urlTechno;

    @Listen("onClick = #addBtn")
    public void addTechno() {
        if (technoEditableText == null || technoList == null) {
            Messagebox.show("Error on this application", "Error", Messagebox.OK, Messagebox.ERROR);
        } else {
            String text = technoEditableText.getValue();
            if (text != null && !text.isEmpty()) {
                if (technoEditableTextDescription != null && technoEditableTextDescription.getValue() != null) {
                    if (technoEditableTextUrl != null && technoEditableTextUrl.getValue() != null) {
                        technos.add(new Techno(text, technoEditableTextDescription.getValue(), technoEditableTextUrl.getValue()));
                    } else {
                        technos.add(new Techno(text, technoEditableTextDescription.getValue()));
                    }
                } else if (technoEditableTextUrl != null && technoEditableTextUrl.getValue() != null) {
                    Techno a = new Techno(text);
                    a.setUrl(technoEditableTextUrl.getValue());
                    technos.add(a);
                } else {
                    technos.add(new Techno(text));
                }
            } else {
                Messagebox.show("The task name is empty", "Warning", Messagebox.OK, Messagebox.EXCLAMATION);
            }
        }
    }

    @Listen("onSelect = #technoList")
    public void selection() {
    	int index = technoList.getSelectedIndex();
        nameTechno.setValue(((Techno) technos.get(index)).getTechnoName());
        descriptionTechno.setValue(((Techno) technos.get(index)).getDescription());
        if (((Techno) technos.get(index)).getUrl() != null) {
            urlTechno.setValue(((Techno) technos.get(index)).getUrl());
        } else {
            urlTechno.setValue("");
        }
    }

    @Listen("onLoad = #technoList")
    public void see() {
        technoList.setItemRenderer(new ListitemRenderer<Techno>() {
            public void render(final Listitem listitem, Techno techno, final int i) throws Exception {
                new Listcell(techno.getTechnoName()).setParent(listitem);
                Button button = new Button("Remove");
                Listcell cell3 = new Listcell();
                button.setParent(cell3);
                cell3.setParent(listitem);

                button.addEventListener("onClick", (event) -> {
                            technos.remove(i);
                            ListModel<Techno> lm = new ListModelList<>(technos, true);
                            technoList.setModel(lm);
                        }
                );
            }
        });
    }

    @Override
    public void doAfterCompose(Component component) throws Exception {
        super.doAfterCompose(component);
        technoList.setModel(technos);
        see();
    }
}
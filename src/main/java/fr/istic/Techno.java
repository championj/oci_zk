package fr.istic;

public class Techno {

    private String technoName;
    private String description;
    private String url;

    public Techno(String name) {
        technoName = name;
    }

    public Techno(String name, String description) {
        technoName = name;
        this.description = description;
    }

    public Techno(String name, String description, String url) {
        technoName = name;
        this.description = description;
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public String getTechnoName() {
        return technoName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
